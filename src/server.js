const app = require('./router');
const port = 3000;

app.listen(port, () => console.log(`Authentication app listening on port ${port}!`));

module.exports = app;
