const request = require('supertest');
const app = require('../../src/router');

const agent = request.agent(app);

module.exports = agent;
