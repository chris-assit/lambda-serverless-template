const expect = require('chai').expect;
const agent = require('./test-agent');

describe('GET /health', () => {
    it('responds with json', () => {
        return agent.get('/')

            .then((response) => {
                expect(response.body).to.have.property('healthy');
                expect(response.body.healthy).to.be.true;
            });
    });
});
