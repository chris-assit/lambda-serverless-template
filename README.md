# Lambda ExpressJS Template project

This serves as a starter template for projects being deployed on Lambda

Commands are contained inside the package.json and are partially as follows:

* `yarn dev` - Run a development server locally
* `yarn watch` - Run a development server locally, and restart when files in `/src` change.
* `yarn package` - Grab the contents of `/src` and `/node_modules` and zip them to upload to lambda or S3.

The preferred way to use this repo is as a start for a new project.

### Project structure.

The project has 3 main files
* `src/router.js` - The app is defined in here with it's endpoints
* `src/server.js` - Uses the router to start up a local server using `app.listen()`
* `src/lambda.js` - Contains the Lambda handler for the server. 

When deploying to lambda the handler will be specified as `src/lambda.handler`

### CI/CD

This project contains a `.gitlab-ci.yml` file as a starting point for Gitlab CI.

On first push to the master branch the pipeline will run, but stop before uploading the packaged service to S3.

The pipeline expects to be part of a group which has `AWS_` environment variables set which have access to upload to S3.

It also assumes a project level variable `S3_BUCKET_NAME`, containing the bucket for where the function should be uploaded.

If your repo is created in the infrastructure repo, all this is automatically done when applied and you can just start coding.

```hcl-terraform
module "my-service-vcs" {
  source = "./service-vcs"
  group_id = "${gitlab_group.linguine-serverless-services.id}"
  service_name = "my-service"
  # The bucket name is not required, but can be used to avoid errors as AWS bucket names are unique
  bucket_name = "my-service-bucket"
}
```

### Testing

The project uses mocha, chai, and supertest for testing.

Supertest is used to test against the API,
Chai can be used for assertions, 
and mocha is the main runner and definition syntax for the tests.

All tests are contained in `/test` to avoid them being packaged for deployment.

There are 2 self-explanatory sub-directories.
* `/test/api` - Supertest tests against the public facing API.
* `/test/unit` - Unit testing for specific functionalities.  

 ### Deployment
 
In order to be easily deployed, a terraform module has been created in the infrastructure repo and the version of your bucket file will be specified there to be deployed. 
 
A typical definition for a service will look something like this:

```hcl-terraform
module "my-service-deployment" {
  source = "./service-deployment"
  service_name = "my-service"
  service_handler = "src/lambda.handler"
  storage_bucket = "${module.my-service-vcs.storage-bucket}"
  storage_key = "function.zip"
  storage_version = "some-aws-bucket-version"
  domain_name = "some-domain-to-my-service.linguine.app"
  cloudflare_zone = "${cloudflare_zone.linguine.id}" # Leave as is
  certificate_validation_arn = "${module.linguine-domain-certificate.certificate_validation}" # Leave as is
}
```
